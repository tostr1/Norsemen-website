# Norsemen-website
Static website for the upcoming game Norsemen by Neo Red Entertainment (WIP)

## About this project / me
As a backend developer, with an interest in design, I took onto developing the website for our upcoming game Norsemen.
Working on the game is a small independent team from multiple countries, such as UK, Germany, Netherlands, etc.

## Goals/Requirements of this project
This project has two main goals. First, to be a landing page and a source of information/promotion for the game.
And secondly, it also has to be responsive, so anyone can easily view the site on their mobile devices without any problems.

## Methods / technologies used
This site could have either been done as a static HTML page, or with a lightweight backend framework underneath.
As I am most familiar with backend systems, I chose node.js paired with the well known express.js framework.
I did this mainly because of the added flexibility to, for example, integrate REST ajax requests down line, or else.

For the styling I chose the renowned Bootstrap 3 CSS framework. Having worked with it quite a bit this was a no brainer for me.
Learning a front-end framework, such as React or Angular, would have been interesting surely, but unnecessary for the scope of this project.

## Development
With these tools I started the process by jotting down a few design sketches. After submitting these and chosing one to our liking I 
started the development of the site. I applied the mobile-first technique, as this forces me to have a working responsive view and build upon the growing
screen resolution of larger devices. This also makes the CSS development a bit easier, as you don't get too many conflicting rules.  
  
## Project status
Currently the page is more of a final prototype, containing only sample images and non finalized text. It will be worked on further, as the ongoing development of the game 
requires/allows it.
